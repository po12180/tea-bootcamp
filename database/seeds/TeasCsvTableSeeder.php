<?php

use Illuminate\Database\Seeder;

use Flynsarmy\CsvSeeder\CsvSeeder;


class TeasCsvTableSeeder extends CsvSeeder {

    public function __construct()
    {
        $this->table = 'teas';
        $this->filename = base_path().'/database/seeds/csv/tea_import.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}