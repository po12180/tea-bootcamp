<?php

use Illuminate\Database\Seeder;

use Flynsarmy\CsvSeeder\CsvSeeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddressesCsvTableSeeder::class);
        $this->call(ReviewsCsvTableSeeder::class);
        // $this->call(TeasTableSeeder::class);
        $this->call(UserTableSeeder::class);

        $this->call(CompaniesCsvTableSeeder::class);
        $this->call(TeasCsvTableSeeder::class);
    }
}

// VERY IMPORTANT TODO
// Didnt manage to link the different files using the CSV seeder.
// Instead, all the CSV readers live inside this file.
// Needs to be refactored.



class TeasCsvTableSeeder extends CsvSeeder {

    public function __construct()
    {
        $this->table = 'teas';
        $this->filename = base_path().'/database/seeds/csv/tea_import.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}



class CompaniesCsvTableSeeder extends CsvSeeder {

    public function __construct()
    {
        $this->table = 'companies';
        $this->filename = base_path().'/database/seeds/csv/company_import.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}


class AddressesCsvTableSeeder extends CsvSeeder {

    public function __construct()
    {
        $this->table = 'addresses';
        $this->filename = base_path().'/database/seeds/csv/address_import.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}


class ReviewsCsvTableSeeder extends CsvSeeder {

    public function __construct()
    {
        $this->table = 'reviews';
        $this->filename = base_path().'/database/seeds/csv/review_import.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();

        parent::run();
    }
}
