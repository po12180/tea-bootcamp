<?php

use Illuminate\Database\Seeder;

use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::create(array(
          'username' => 'pieteroliver',
          'first_name' => 'Pieter',
          'last_name' => 'Oliver',
          'email' => 'name@domain.com',
          'password' => Hash::make('qwerty'),
          'image_file' => '001' ,
          'image_extension' => '.jpg',
        ));

        $faker = Faker\Factory::create();
         
        for ($i = 2; $i < 60; $i++)
        {
          $user = User::create(array(
            'username' => $faker->userName,
            'first_name' => $faker->firstName,
            'last_name' => $faker->lastName,
            'email' => $faker->email,
            'password' => $faker->word,
            'image_file' => sprintf("%03d", $i) ,
            'image_extension' => '.jpg',
            // 'password' => Hash::make('$faker->word') // This is best practise.
          ));
        }

    }
}



