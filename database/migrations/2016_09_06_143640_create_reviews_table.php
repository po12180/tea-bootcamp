<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');

            // This needs to be done such only one can be chosen.
            // TODO extend to tea equipment at some point.
            $table->unsignedInteger('tea_id');
            // $table->unsignedInteger('company_id')->nullable();

            $table->Integer('rating'); // 1 to 100, 
            $table->text('content');
            $table->boolean('verified'); //review has been checked.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
