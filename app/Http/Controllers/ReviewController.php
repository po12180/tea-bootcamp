<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// use App\Review;
use App\Http\Requests;
use App\Review;
use App\Tea;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.teas
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    public function __construct()
    {
        // magic suggested by ryan, lets only logged in users see the create page.
        $this->middleware('auth', ['only' => ['create']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tea = Tea::find($id);
        // dd($tea);
        return view('reviews.create', compact('tea'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // 1. Validate the data
        $this->validate($request, [
            'tea_id' => 'required',
            'rating' => 'required|numeric|min:0|max:100',
            'content' => 'required|min:4|max:16000'
        ]);

        // Extracts and validates.

        $review = Review::create([
            'tea_id' => $request->input('tea_id'),
            'user_id' => Auth::user()->id,
            'rating' => $request->input('rating'),
            'content' => $request->input('content'),
            'verified' => '1',
        ]);

        // 4. Redirect to a view.
        // TOLDO 
        return redirect()->route('tea.show', ['id' => $request->input('tea_id')]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if ($review = Review::find($id)){
            // $company = Company::find($tea->company_id);
            return view('reviews.show', compact('review'));
        }
        else{
            // return view('errors.teaNotFound');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Review $review)
    {

        // example by nathan.
        $review->name = 'thing';
        $review->save();

        $review->update(['name' => 'thing']);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function verified()
    {
        return view('reviews.verified');
    }


}
