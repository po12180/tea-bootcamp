<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $users = $users->sortByDesc('NumberOfReviews');
        // $users->average = round($tea->reviews()->avg('rating'));
        return view('user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     // Create a new location,

    //     // 1. Extract the form data.
    //     return $request->get('title'); //even if its a post, get will work.

    //     $this->validate($request, [
    //         'title' => 'required|unique:posts|max:255',
    //         'body' => 'required',
    //     ]);

    //     // 2. Validate the data.

    //     // 3. Write to the database.

    //     // 4. Redirect somehow


    // }



    public function show($id)
    {
        // return view('user.show', compact('user'));
        if ($user = User::find($id)){
            return view('user.show', compact('user'));
        }
        else{
            return view('errors.userNotFound');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function reviews()
    {
        return 123;
    }



}
