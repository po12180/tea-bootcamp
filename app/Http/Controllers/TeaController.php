<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Tea;
use App\Company;
use App\Http\Requests;

class TeaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teas = Tea::all();
        $teas = $teas->sortByDesc('AverageRating');

        return view('teas.index',compact('teas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $slug = null)
    {

        // handles the two different types of pages.
        if ($tea = Tea::find($id)){

            // Added the reviews for the item.
            $reviews = Tea::find($id)->reviews;
            // $tea->average = round($tea->reviews()->avg('rating'));

            // adam showed me this way of adding a slug.
            return $tea->slug == $slug
                ? view('teas.show', compact('tea','reviews'))
                : redirect($tea->url);
        }
        else{
            return view('errors.teaNotFound');
        }
    }

// public function show(Country $country, $slug = null)

//     {

//         return $country->slug == $slug

//             ? view('search.cities', compact('country'))

//             : redirect($country->url);

//     }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
