<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'first_name',
        'last_name',
        'password',
        'image_file',
        'image_extension',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function getImageLocationAttribute()
    {
        return "/images/user/". $this->image_file . $this->image_extension;
    }

    public function getNumberOfReviewsAttribute()
    {
        return count($this->reviews);
    }

}
