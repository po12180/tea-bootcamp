<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tea extends Model
{

    // lets you edit these things.
    protected $fillable = [
        'name',
        'type',
        'company_id',
        'image_file',
        'image_extension',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }

    public function getAverageRatingAttribute()
    {
        return round($this->reviews()->avg('rating'));
    }

    public function getImageLocationAttribute()
    {
        return "/images/tea/". $this->image_file . $this->image_extension;
    }

    public function getUrlAttribute()
    {
        return route('tea.show', ['id' => $this->id, 'slug' => $this->slug]);
    }

public function getSlugAttribute()
    {
        return str_slug($this->name);
    }


}