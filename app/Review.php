<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    // These things can be filled in the database.
    protected $fillable = [
        'tea_id',
        'user_id',
        'content',
        'rating',
        'verified',
    ];

    public function tea()
    {
        return $this->belongsTo('App\Tea');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }




}
