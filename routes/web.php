<?php

use App\Tea;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


    // TOLDO rewrite to a controller, have it 
Route::get('/', function () {
    // TODO add top four teas.
    $teas = Tea::all()->sortByDesc('AverageRating')->take(4);
    $users = User::all()->sortByDesc('NumberOfReviews')->take(4);
    return view('pages.index', compact('teas','users'));
})->name('home');


Route::resource('tea', 'TeaController',
    ['except' => 
    ['show']
    ]);

Route::get('tea/{tea}/{slug?}', 'TeaController@show')->name('tea.show');

Route::resource('review', 'ReviewController',
    ['except' => 
    ['create']
    ]);

Route::get('review/create/{id}', 'ReviewController@create')->name('review.create');

Route::resource('user', 'UserController');

Route::get('pages/verified', function () {
    return view('pages.verified');
})->name('verified_review');


Route::get('about_us', function (){
    return view('pages.about_us');
})->name('about_us');
Route::get('faq', function (){
    return view('pages.faq');
})->name('faq');
Route::get('terms_and_conditions', function (){
    return view('pages.tnc');
})->name('tnc');


Route::get('phpinfo', function () {
    return phpinfo();
});



Route::get('/home', 'HomeController@index');

// Let a user add a tearoom (innit).
Route::get('tearoom/main','TearoomController@create');

Route::post('tearoom', 'TearoomController@store');

Route::get('api/v2/users/{user}/reviews', 'UserController@reviews')->name('users.reviews');

Auth::routes();