@extends('layouts.app')

@section('content')

@if (Auth::check())
    <h1> Here are some fellow users! </h1>
@else
    <h1> Join Our Growing User Base!</h1>
@endif


    <div class="row">
        @foreach ($users as $user)
            <div class="col-xs-6 col-sm-4 col-md-3">
                <a href="{{ route('user.show', $user->id) }}" class="thumbnail">
                    <img src="{{ url($user->ImageLocation) }}" alt="{{ $user->username }}'s' avatar image">
                </a>
                <div class="caption text-center">
                    <h3> {{ $user->username }} </h3>
                    <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>  {{ count($user->reviews) }}
                </div>
            </div>
        @endforeach
    </div>

@stop
