@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-xs-8">
            <h1> {{ $user->username }}</h1>
            <img src="{{ url($user->ImageLocation) }}" alt="{{ $user->username }}'s' avatar image">
        </div>
        <div class="col-xs-4">
        </div>
    </div>


<!--     <h2>This is {{$user->first_name }} {{$user->last_name}}. </h2>
 -->

<h2>Reviews</h2>


@if (Auth::user() && $user->id == Auth::user()->id && $user->NumberOfReviews == 0)
    <p>Why not have a look?</p>
@elseif ($user->NumberOfReviews == 0)
    <h4>
        It seems that this user has not posted anything.
    </h4>
@endif



@foreach ( $user->reviews as $review )

    <div class="panel panel-default">
        <div class="panel-heading">
                <img src="{{ url($review->tea->ImageLocation) }}" alt="Image of {{ $review->tea->name }}">
                <a href="{{route('tea.show',['id' => $review->tea])}}">{{$review->tea->name}}</a>
                Rating: {{ $review->rating }}


        </div>
        <div class="panel-body">
            {{$review->content}}
        </div>
    </div>

@endforeach


@stop
