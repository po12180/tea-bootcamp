@extends('layouts.app')

@section('content')

    <h1> Tea Not Found </h1>

    <p>
        It seems like this tea doesn't exist any more or has yet to be added.
        Maybe you would like to add this tea?
    </p>

    <p>
        Insert tea adding form here.
    </p>

    <!-- TODO ADD A TEA INPUT FORM. -->


    <a href="{{route('home')}}"> Click here to go home.</a> 
    <br>
    <a href="#" onclick="javascript:history.go(-1);">Go back to the previous page</a>



@stop
