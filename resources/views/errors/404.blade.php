@extends('layouts.app')

@section('content')

    <h1> 404 Error </h1>

    <p>
        This pages has not been found.
        This is not meant to happen.
    </p>

    <a href="{{route('home')}}"> Click here to go home.</a> 
    <br>
    <a href="#" onclick="javascript:history.go(-1);">Go back to the previous page</a>

@stop