@extends('layouts.app')

@section('content')

<p>
    Please add a review for an existing tea. 
    If you cannot find it, then have a look at <a href="{{route('tea.create')}}">here</a>.
</p>


    <form class="form-horizontal" action="{{ route('review.store') }}" method="post">

        <!-- ensure that the form can pass CSRF protection -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="tea_id" class="col-sm-2 control-label">Tea reviewed:</label>
            <div class="col-sm-10">
              <input name="tea_id" type="hidden" class="form-control" id="tea_id" value="{{$tea->id}}">
                <h5>{{$tea->name}} by {{$tea->company->name}}</h5>
            </div>
        </div>

        <div class="form-group">
            <label for="rating" class="col-sm-2 control-label">Rating (0-100)</label>
            <div class="col-sm-10">
                <input name="rating" type="number" id="replyNumber" min="0" step="1" max="100" data-bind="value:replyNumber" />
            </div>
        </div>

        <div class="form-group">
            <label for="content" class="col-sm-2 control-label">Review Text</label>
            <div class="col-sm-10">
              <textarea name="content" type="text" class="form-control" id="content" placeholder="Review, decribe your experience." maxlength="16000" minlength="10"></textarea>
            </div>
        </div>
    
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-primary">Create!</button>
            </div>
        </div>

    </form>

@stop