@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-xs-12">
        <h2>About {{ config('app.name', 'Laravel') }}</h2><br>

        <img style="width:100px" src='/images/user/pieter.jpg' alt="Image of the best developer ever, Pieter Oliver.">

        <h4>
            Pieter coined the idea as he was drinking some tea. 
            We have top tea scientists working for us to analyse tea.
        </h4>
        <p>
            He looked around and wanted to inspire people to try more varied tea.
            The irony is his favourite tea is a fast black tea with milk.
            He does enjoy a good green tea, camomile, oolong and mint when time permits.
        </p>
    </div>
</div>

@endsection