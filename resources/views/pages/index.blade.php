@extends('layouts.app')

@section('heading')
  <link href="/css/main_page.css" rel="stylesheet">
@endsection


@section('top')


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <div class="container">
            <div class="carousel-main-page">
              <h1><br><br><br></h1> <!-- This is very hackey, took 30min to not do. -->
              <h1>Tea-Share</h1>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container carosel-daddy">
            <div class="carousel-main-page">
              <div>
                <h1><br><br><br></h1>
                <h3>
                  Share your experiences with the different teas you have.
                </h3>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="container carosel-daddy">
            <div class="carousel-main-page">
              <h1><br><br><br></h1>
              <h2>Share you favourite teas on social media</h2>
              <h4>Twitter, Facebook and Google+!</h4>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->


@endsection

@section('content')

<div class="jumbotron">
  <h1>
    @if (Auth::user())
      Discover more teas to add to your favourites.
    @else
      Find an interesting tea!
    @endif
  </h1>

    <div class="row">
        @foreach ($teas as $tea)
            <div class="col-xs-6 col-sm-4 col-md-3 tea-box">
                <a href="{{ $tea->url }}" class="thumbnail">
                    <img src='{{$tea->ImageLocation}}' alt="image of {{ $tea->name }}" class="tea_image">
                </a> 
                <div class="caption">
                    {{ $tea->name }}
                    is a {{ $tea->type }} tea, 
                    by <a href="{{$tea->company->url}}" target="_blank">{{$tea->company->name }}</a>.
                    
                    <h3 class="text-center">
                        {{count($tea->reviews)}} 
                        <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                    </h3>
                </div>


            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{$tea->averageRating}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$tea->averageRating}}%;">
                    {{$tea->averageRating}}
                </div>
            </div>

            </div>
        @endforeach
    </div>
</div>


<div class="jumbotron">
  <h1>
    @if (Auth::user())
      Interact with other users!
    @else
      Join our active users!
    @endif
  </h1>

    <div class="row">
        @foreach ($users as $user)
            <div class="col-xs-6 col-sm-4 col-md-3">
                <a href="{{ route('user.show', $user->id) }}" class="thumbnail">
                    <img src="{{ url($user->ImageLocation) }}" alt="{{ $user->username }}'s' avatar image">
                </a>
                <div class="caption">
                    <h3> {{ $user->username }} </h3>
                    Reviews  {{ count($user->reviews) }}
                </div>
            </div>
        @endforeach
    </div>
</div>



@endsection