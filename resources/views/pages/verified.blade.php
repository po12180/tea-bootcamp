@extends('layouts.app')

@section('content')

<h1>
    What is a verified review?
</h1>

<p>
    A verified review is one where we can guarantee that the item in question was purchased by the user. 
    We trust users are disclosing any financial interests.
    As per the terms of service, users found posting fake reviews will be named, shame and highlighted as big fat phonies!
</p>


@stop