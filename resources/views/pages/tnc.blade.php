@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-xs-12">

        <div class="page-header">
                <h2>1. General Terms</h2>
        </div>
        <p><strong>1.1</strong> 
            By accessing and using {{ config('app.name', 'Laravel') }}, you confirm that you are in agreement with and bound by the terms and conditions contained in the Terms Of Use outlined below. 
            These terms apply to the entire website and any email or other type of communication between you and {{ config('app.name', 'Laravel') }}.
        </p>

        <div class="page-header term">
                <h2>2. Users</h2>
        </div>
        <p><strong>2.1</strong> All users of {{ config('app.name', 'Laravel') }} will be nice to each other. If you wouldn't say it to your mother, don't say it here.</p>
        <p><strong>2.2</strong> {{ config('app.name', 'Laravel') }} is not responsible for any review by users, we will try to check that there are not financial interests undisclosed but this is a challenge.</p>


        <div class="page-header term">
                <h2>3. Security</h2>
        </div>
        <p><strong>3.1</strong> {{ config('app.name', 'Laravel') }} securely stores your passwords and details. Feel free to contact us about our security policies.</p>
        
        <div class="page-header term">
                <h2>4. Reviews</h2>
        </div>
        <p><strong>4.1</strong> As stated on our <a href="{{ route('verified_review') }}">verified review</a> page, {{ config('app.name', 'Laravel') }}
        tries to weed out bias, rude and false reviews. If any slip through, let us know.</p>

        <div class="page-header term">
                <h2>5. Admin Privileges</h2>
        </div>
        <p>
            <strong>5.1</strong> As the dictator at the helm, I reserve the right to settle disputes between users as I see fit.
            If it requires me to go <a href="https://en.wikipedia.org/wiki/Judgment_of_Solomon">King Solomon</a>, so be it.
        </p>


        <div class="page-header changes">
                <h2>Changes to terms</h2>
        </div>
        <p>If we change our terms of use we will post those changes on this page. Registered users might be sent an email that outlines changes made to the terms of use, if Pieter add that feature.</p>
    </div>
</div>


<!-- TOLDO Add a better terms of conditions. -->

<!-- 
    a licence of the copyright in the website (and restrictions on what may be done with the material on the website);
    a disclaimer of liability;
    a clause governing the use of passwords and restricted areas of the website;
    an acceptable use clause;
    a variation clause;
    an entire agreement clause;
    a clause specifying the applicable law and the jurisdiction in which disputes will be decided; and
    a provision specifying some of the information which needs to be disclosed under the Ecommerce Regulations.
 -->



@endsection