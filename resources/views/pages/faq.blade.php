@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-xs-12">
        
        <h1>Frequently Asked Questions</h1>


<!-- Shamelessly adapted from www.prepbootstrap.com/bootstrap-template/faq-example -->

    <div class="panel-group" id="accordion">
        <div class="faqHeader"><h3>General questions</h3></div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Is account registration required?</a>
                </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                    Account registration at <strong>{{ config('app.name', 'Laravel') }}</strong> is only required if you will be sharing experiences and adding teas you like. 
                    This has mostly been done so you can keep track of your teas, meet and chat with others with a persistent username.
                    We do this to make you happy.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">Can I submit teas that are not listed?</a>
                </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse">
                <div class="panel-body">
                    Of course you can! Look <a href="{{route('tea.create')}}">here</a> for a form where you can add teas.
                    Make sure you've had a search before you add an existing tea, we try out best to add a decent search feature,
                    if you have any feedback send an email to the <a href="mailto:admin@teashare.com?Subject=TeaStore%20Feedback" target="_top">Admin</a>.
                    We value your feedback as a user.
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven">
                        Why bother log interesting teas I've drank?
                    </a>
                </h4>
            </div>
            <div id="collapseEleven" class="panel-collapse collapse">
                <div class="panel-body">
                    Why not? If you enjoyed something why would you not share it?
                    77% of people  <!-- TODO turn this into a modal citation that looks good. -->
                    <sup>
                        <button type="button" class="btn" data-toggle="modal" data-target="#myModal">
                          1
                        </button>
                    </sup>
                    review something before buying it online, 
                    why not help your community by contributing to it's collective palate.
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        Cited from <a href="http://www.business.com/online-marketing/77-percent-of-people-read-online-reviews-before-buying-are-they-finding-you/">
            buisness.com
        </a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>



    </div>
</div>

@endsection