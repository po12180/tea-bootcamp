@extends('layouts.app')

@section('content')

<p>
    Please add a tea which our website doesn't have.
    Have a search on our site for it first.
</p>

    <!-- TODO add a search bar in here to search existing teas. -->

    <form class="form-horizontal" action="{{ route('tea.store') }}" method="post">

        <!-- ensure that the form can pass CSRF protection -->
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input name="name" type="text" class="form-control" id="name" placeholder="Name">
            </div>
        </div>

        <div class="form-group">
            <label for="company_id" class="col-sm-2 control-label">Company ID</label>
            <div class="col-sm-10">
              <input name="company_id" type="replyNumber" class="form-control" id="company_id" placeholder="ID of company">
            </div>
        </div>

        <div class="form-group">
            <label for="type" class="col-sm-2 control-label">Tea Type</label>
            <div class="col-sm-10">
              <input name="type" type="text" class="form-control" id="type" placeholder="Description">
            </div>
        </div>
    
        <div class="form-group">
            <div class="col-sm-10 col-sm-offset-2">
                <button class="btn btn-primary">Create!</button>
            </div>
        </div>

    </form>



@stop