@extends('layouts.app')

@section('content')


<!-- This is now a landing page for all the teas we have offer. -->

<h1>Explore our highly rated teas</h1>

<h2>We have {{count($teas)}} teas on our site!</h2>

<h3>
    Can't find a tea you want to review?

    @if (Auth::check())
        <a href="{{ route('tea.create') }}">
            <button class="btn" >
                Add a tea<span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
            </button>
        </a>
    @else
        Join us, <a href="{{ url('/register') }}">Sign Up</a>
    @endif
</h3>




<div class="row">
    @foreach ($teas as $tea)
        <div class="col-xs-6 col-sm-4 col-md-3 tea-box">
            <a href="{{ $tea->url }}" class="thumbnail">
                <img src='{{$tea->ImageLocation}}' alt="image of {{ $tea->name }}" class="tea_image">
            </a> 
            <div class="caption">
                {{ $tea->name }}
                is a {{ $tea->type }} tea, 
                by <a href="{{$tea->company->url}}" target="_blank">{{$tea->company->name }}</a>.
                
                <h3 class="text-center">
                    {{count($tea->reviews)}} 
                    <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                </h3>
            </div>

        <div class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="{{$tea->averageRating}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$tea->averageRating}}%;">
                {{$tea->averageRating}} 
            </div>
        </div>

        </div>
    @endforeach
</div>

@endsection
