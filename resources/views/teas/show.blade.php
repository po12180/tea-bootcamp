@extends('layouts.app')

@section('content')

<div class="container-fluid">
<div class="row">
    <div class="col-xs-12 col-md-6">
        <h1> {{ $tea->name }}</h1>

        <h2>Made by <a href="{{$tea->company->url}}">{{$tea->company->name }}</a></h2>

        <h3> Type of Tea : {{ $tea->type  }}</h3>

        <h3> Average Tea Rating: {{ $tea->averageRating }} / 100</h3>

        <h3>
            Buy 
            <a href="{{$tea->buy_url}}" target="_blank">
                <button  class="btn btn-primary btn-sm" >
                    <i class="fa fa-coffee" aria-hidden="true"></i>
                </button>
            </a>
            from a 3rd party retailer
        </h3>

            <a href="https://twitter.com/share" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

            <!-- Made the button by fiddling in css heavily. -->
            <!-- http://stackoverflow.com/questions/16463030/how-to-add-facebook-share-button-on-my-website -->
            <a href="https://www.facebook.com/sharer/sharer.php?u={{Request::fullurl()}}" target="_blank" class='fb_button'>
                Share on Facebook
            </a>
            <!-- https://developers.google.com/+/web/share/ Had to fiddle in css again, now is same height as the other two. -->
            <a href="https://plus.google.com/share?url={{Request::url()}}" onclick="javascript:window.open(this.href,
              '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img
              src="/images/gplus-21.png" alt="Share on Google+" class="google-btn"/></a>
    </div>
    <div class="col-xs-12 col-md-6 tea-image-box">
        <img src='{{$tea->ImageLocation}}' alt="image of {{ $tea->name }}">
    </div>
</div>

</div>


    <!-- Button for sharing on twitter. -->
    <!-- $reviews = Tea::all()->reviews; -->

    @foreach ($reviews as $review) 

        <div class="panel panel-default">
            <div class="panel-heading">
                <img src="{{ url($review->user->ImageLocation) }}" alt="{{ $review->user->username }}'s' avatar image">
                <a href="{{route('user.show',['id' => $review->user])}}">{{$review->user->username}}</a>
                Rating: {{ $review->rating }} / 100

                @if ($review->verified == 1)
                <div class="pull-right">
                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#verifiedModal">
                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                    </button>
                    Verified review
                </div>
                @endif

            </div>
            <div class="panel-body">
                {{ $review->content }}
            </div>
        </div>
<!-- 
        <h3>Review by {{$review->user->username}}</h3>

        <h3> {{ $review->content }}</h3>
 -->
        <!-- {{$review}} -->

    @endforeach




<!-- Modal for the verified button.-->
<div class="modal fade" id="verifiedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
          What is a verified review?
        </h4>
      </div>
      <div class="modal-body">
        A verified review is one where we can guarantee that the item in question was purchased by the user.
        We cannot yet verify that users are disclosing any financial interests.
        Adding a simple disclaimer to your review is enough
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="{{ route('verified_review') }}" class="btn btn-info" role="button">Find Out More</a>
      </div>
    </div>
  </div>
</div>

<!-- Bottom section to give the users a link to post reviews but sign up if not logged in. -->
<div class="text-center">
    <h3>
        Would you like to add your review here?
    </h3>
    <br>
    @if (Auth::check())
        <a href="{{ route('review.create', ['id' => $tea->id])}}">
            <button type="button" class="btn btn-default btn-lg">
                    <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
            </button>
        </a>
    @else
        <h2> 
            <a href="{{ url('/register') }}">Sign Up</a>
            /
            <a href="{{ url('/login') }}">Login</a>
        </h2>
    @endif
</div>
<br>

@stop
